const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const artist = await Artist.create(
      {name: "TR/ST", photo: "trst.jpg", info: "trst info"},
      {name: "Queen", photo: "queen.jpg", info: "queen info"},
  );

  const albums = await Album.create(
    {name: "Destroyer-1", year: 2019, artist: artist[0]._id, photo: "Destroyer-1.jpg"},
    {name: "A Night at the Opera", year: 1975, artist: artist[1]._id, photo: "A Night at the Opera.jpg"},
  );

  const tracks = await Track.create(
      {name: "Unbleached", album: albums[0]._id, duration: "03:45"},
      {name: "Gone", album: albums[0]._id, duration: "03:48"},
      {name: "Love of my life", album: albums[1]._id, duration: "03:48"},
      {name: "Bohemian Rhapsody", album: albums[1]._id, duration: "06:48"},
  );

  await connection.close();
};

run().catch(error => {
  console.error('Something went wrong', error);
});
