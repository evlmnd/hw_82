const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Artist = require('../models/Artist');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Artist.find()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});


router.post('/', upload.single('image'), (req, res) => {
    const data = {
        name: req.body.name,
        info: req.body.info
    };

    if (req.file) {
        data.photo = req.file.filename;
    }

    const artist = new Artist(data);
    artist
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});


module.exports = router;
