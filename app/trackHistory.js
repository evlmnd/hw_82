const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Token');
    if (!token) res.status(401).send({message: 'No token here'});

    const user = await User.findOne({token: token});
    if (user) {
        const trackHistory = new TrackHistory({
            user: user._id,
            track: req.body.track,
            dateTime: new Date()
        });
        res.send(trackHistory);
    } else {
        res.status(401).send({message: 'Unauthorized'});
    }
});

module.exports = router;