const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.album) {
        Track.find({artist: req.query.album})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            });
    } else {
        Track.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    }
});


router.post('/', (req, res) => {
    const data = req.body;

    const album = new Track(data);
    album
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});

module.exports = router;
